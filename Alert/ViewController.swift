//
//  ViewController.swift
//  Alert
//
//  Created by Rumeysa Bulut on 10.11.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var password2Text: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordText.isSecureTextEntry = true
        password2Text.isSecureTextEntry = true
        // Do any additional setup after loading the view.
    }

    @IBAction func signUp(_ sender: Any) {
        
        if usernameText.text == "" {
            showAlert(title: "Error", message: "User not found.")
        } else if passwordText.text == "" {
            showAlert(title: "Error", message: "Password not found.")
        } else if passwordText.text != password2Text.text {
            showAlert(title: "Error", message: "Passwords do not match.")
        } else {
            showAlert(title: "Success", message: "User OK")
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (UIAlertAction) in
            print("ok button clicked")
        }
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
}

